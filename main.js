Vue.component('modal', {
    template: '#modal-template'
  })
  
new Vue({
    el: 'body',
    data: {
        showModal: false,
        products: [{            
            "Name": "Мячи",
            "Count": 12,
            "Price": 999
        }, {            
            "Name": "Шайбы",
            "Count": 15,
            "Price": 99
        }, {            
            "Name": "Ракетки",
            "Count": 14,
            "Price": 1299
        }],
        sum: 0
    },
    methods: {
        open: function() {
            this.active = true
        },
        close: function() {
            this.active = false
        },
        onCancel: function() {
            this.close();
        },
        onConfirm: function() {
            this.close();
        }
    }
})